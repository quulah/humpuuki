from device.ruuvi import RuuviDevice


def test_ruuvi_parse():
    device = RuuviDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    raw = b"\x02\x01\x06\x1b\xff\x99\x04\x05\x02\x82\x95k\xc5w\xfc\xbc\xfd\xbc\xff\xf8b\xb6\x9f\xb4\xf8\xd32\x9ed\xc9\x0f"

    device.parse(raw)

    assert f"{device.temperature}" == "3.21 °C"
    assert f"{device.humidity}" == "95.6275 %"
    assert f"{device.pressure}" == "1005.51 Pa"
    assert f"{device.acceleration_x}" == "-0.836 G"
    assert f"{device.acceleration_y}" == "-0.58 G"
    assert f"{device.acceleration_z}" == "-0.008 G"
    assert f"{device.battery_voltage}" == "2.389 V"
    assert f"{device.tx_power}" == "-18 dBm"
    assert f"{device.movement_counter}" == "159"
    assert f"{device.measurement_sequence}" == "46328"

    assert f"{device.equilibrium_vapor_pressure}" == "769.1858611639334 Pa"
    assert f"{device.absolute_humidity}" == "5.768700340208616 g/m³"
    assert f"{device.dew_point}" == "2.5791468210163444 °C"
    assert f"{device.air_density}" == "0.009174140199773678 kg/m³"


def test_ruuvi_str():
    device = RuuviDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    device.battery_voltage.value = 2.293
    device.temperature.value = 12.3
    device.movement_counter.value = 0
    device.low_battery.value = True

    assert (
        str(device)
        == "test (CA:FE:CA:FE:CA:FE): temperature=12.3 °C, battery_voltage=2.293 V, movement_counter=0, low_battery=ON"
    )


def test_ruuvi_battery():
    device = RuuviDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    raw = b"\x02\x01\x06\x1b\xff\x99\x04\x05\x02\x82\x95k\xc5w\xfc\xbc\xfd\xbc\xff\xf8b\xb6\x9f\xb4\xf8\xd32\x9ed\xc9\x0f"

    device.parse(raw)

    assert device.battery_voltage.value == 2.389
    assert device.low_battery.value == False

    device.low_battery_limit = 2.4

    device.parse(raw)

    assert device.low_battery.value == True


def test_ruuvi_movement():
    device = RuuviDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    raw = b"\x02\x01\x06\x1b\xff\x99\x04\x05\x02\x82\x95k\xc5w\xfc\xbc\xfd\xbc\xff\xf8b\xb6\x9f\xb4\xf8\xd32\x9ed\xc9\x0f"

    device.movement_counter.value = 123
    device.parse(raw)

    assert device.movement_counter.value == 159
    assert device.movement_detected.value == True


def test_ruuvi_reset():
    device = RuuviDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    device.battery_voltage.value = "2.345"
    device.temperature.value = "12.3"
    device.movement_counter.value = "123"

    device.reset()

    assert device.battery_voltage.value == None
    assert device.temperature.value == None
    assert device.movement_counter.value == None
