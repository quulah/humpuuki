from device.miflora import MiFloraDevice


def test_miflora_parse():
    device = MiFloraDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    raw = b"\x1f-3.2.4\xda\x00i\xa3\x00\x00\x0041\x03\x02<\x00\xfb4\x9b"

    device.parse(raw)

    assert f"{device.battery}" == "31 %"
    assert f"{device.temperature}" == "21.8 °C"
    assert f"{device.light}" == "163 lux"
    assert f"{device.moisture}" == "52 %"
    assert f"{device.conductivity}" == "817 µS/cm"


def test_miflora_parse_zeroes():
    device = MiFloraDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    raw = b"\x1b-3.2.4\xe5\x00i\r\x00\x00\x00\x00\x00\x00\x02<\x00\xfb4\x9b"

    device.parse(raw)

    assert f"{device.battery}" == "27 %"
    assert f"{device.temperature}" == "22.9 °C"
    assert f"{device.light}" == "13 lux"
    assert f"{device.moisture}" == "0 %"
    assert f"{device.conductivity}" == "0 µS/cm"


def test_miflora_battery():
    device = MiFloraDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    raw = b"\x05-3.2.4\xda\x00i\xa3\x00\x00\x0041\x03\x02<\x00\xfb4\x9b"

    device.parse(raw)

    assert device.battery.value == 5
    assert device.low_battery.value == True


def test_miflora_str():
    device = MiFloraDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    device.battery.value = 3
    device.temperature.value = 31.2
    device.conductivity.value = 0
    device.low_battery.value = True

    assert (
        f"{device}"
        == "test (CA:FE:CA:FE:CA:FE): battery=3 %, temperature=31.2 °C, conductivity=0 µS/cm, low_battery=ON"
    )


def test_miflora_reset():
    device = MiFloraDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    device.battery.value = "3"
    device.temperature.value = "31.2"
    device.conductivity.value = "12345"

    device.reset()

    assert device.battery.value == None
    assert device.temperature.value == None
    assert device.conductivity.value == None


# b'#-3.2.4\xe4\x00i\x08\x00\x00\x00\x05\x86\x00\x02<\x00\xfb4\x9b'
# b'&-3.2.4\xc5\x00iG\x00\x00\x00\x12U\x00\x02<\x00\xfb4\x9b'
# b'"-3.2.4\xd4\x00i\xa4\x00\x00\x0030\x03\x02<\x00\xfb4\x9b'
