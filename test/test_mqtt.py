import logging
from unittest.mock import MagicMock, Mock, PropertyMock, call, patch

import pytest
from addict import Dict as Addict
from pydantic import BaseModel

from backend.mqtt import MqttBackend
from device.base import BinarySensor, HumpuukiDevice, Sensor
from device.miflora import MiFloraDevice
from device.ruuvi import RuuviDevice

log = logging.getLogger("humpuuki")


@patch("config._c")
@patch("paho.mqtt.client.Client")
def test_mqtt_config(mock_mqtt, mock_c):
    class TestDevice(HumpuukiDevice):
        manufacturer: str = "Test"
        model: str = "Unit"

        foo: Sensor = Sensor(unit="%")
        bar: Sensor = Sensor()
        baz: BinarySensor = BinarySensor()

    mock_c.devices.return_value = [
        TestDevice(name="test", address="CA:FE:CA:FE:CA:FE"),
        TestDevice(name="test2", address="CA:FE:CA:FE:CA:FE"),
    ]

    mqtt = MqttBackend(host="127.0.0.1", topic_prefix="testing")
    mqtt.configure()

    mqtt._client.connect.assert_has_calls([call("127.0.0.1", 1883)])
    mqtt._client.publish.assert_has_calls(
        [
            call(
                "testing/sensor/test_CA-FE-CA-FE-CA-FE_foo/config",
                payload='{"state_topic": "testing/sensor/test_CA-FE-CA-FE-CA-FE/state", "unique_id": "test_CA-FE-CA-FE-CA-FE_foo", "name": "test_test_foo", "device": {"identifiers": ["CA:FE:CA:FE:CA:FE"], "manufacturer": "Test", "model": "Unit", "name": "test"}, "unit_of_measurement": "%", "value_template": "{{ value_json.foo }}"}',
                retain=True,
            ),
            call(
                "testing/sensor/test_CA-FE-CA-FE-CA-FE_bar/config",
                payload='{"state_topic": "testing/sensor/test_CA-FE-CA-FE-CA-FE/state", "unique_id": "test_CA-FE-CA-FE-CA-FE_bar", "name": "test_test_bar", "device": {"identifiers": ["CA:FE:CA:FE:CA:FE"], "manufacturer": "Test", "model": "Unit", "name": "test"}, "unit_of_measurement": null, "value_template": "{{ value_json.bar }}"}',
                retain=True,
            ),
            call(
                "testing/binary_sensor/test_CA-FE-CA-FE-CA-FE_baz/config",
                payload='{"state_topic": "testing/binary_sensor/test_CA-FE-CA-FE-CA-FE_baz", "unique_id": "test_CA-FE-CA-FE-CA-FE_baz", "name": "test_test_baz", "device": {"identifiers": ["CA:FE:CA:FE:CA:FE"], "manufacturer": "Test", "model": "Unit", "name": "test"}, "value_template": "{{ value_json.state }}"}',
                retain=True,
            ),
            call(
                "testing/sensor/test2_CA-FE-CA-FE-CA-FE_foo/config",
                payload='{"state_topic": "testing/sensor/test2_CA-FE-CA-FE-CA-FE/state", "unique_id": "test2_CA-FE-CA-FE-CA-FE_foo", "name": "test_test2_foo", "device": {"identifiers": ["CA:FE:CA:FE:CA:FE"], "manufacturer": "Test", "model": "Unit", "name": "test2"}, "unit_of_measurement": "%", "value_template": "{{ value_json.foo }}"}',
                retain=True,
            ),
            call(
                "testing/sensor/test2_CA-FE-CA-FE-CA-FE_bar/config",
                payload='{"state_topic": "testing/sensor/test2_CA-FE-CA-FE-CA-FE/state", "unique_id": "test2_CA-FE-CA-FE-CA-FE_bar", "name": "test_test2_bar", "device": {"identifiers": ["CA:FE:CA:FE:CA:FE"], "manufacturer": "Test", "model": "Unit", "name": "test2"}, "unit_of_measurement": null, "value_template": "{{ value_json.bar }}"}',
                retain=True,
            ),
            call(
                "testing/binary_sensor/test2_CA-FE-CA-FE-CA-FE_baz/config",
                payload='{"state_topic": "testing/binary_sensor/test2_CA-FE-CA-FE-CA-FE_baz", "unique_id": "test2_CA-FE-CA-FE-CA-FE_baz", "name": "test_test2_baz", "device": {"identifiers": ["CA:FE:CA:FE:CA:FE"], "manufacturer": "Test", "model": "Unit", "name": "test2"}, "value_template": "{{ value_json.state }}"}',
                retain=True,
            ),
        ]
    )


@patch("config.HumpuukiConfig")
@patch("paho.mqtt.client.Client")
def test_mqtt_publish(mock_mqtt, mock_c):
    mqtt = MqttBackend(host="127.0.0.1", port=1883, topic_prefix="homeassistant")
    mqtt.configure()

    plant = MiFloraDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    plant.battery.value = 3
    plant.temperature.value = 31.2
    plant.conductivity.value = 123

    ruuvitag = RuuviDevice(name="test2", address="CA:FE:CA:FE:CA:FE")

    ruuvitag.battery_voltage.value = 2.345
    ruuvitag.temperature.value = 12.3
    ruuvitag.movement_counter.value = 123

    mqtt.publish([plant, ruuvitag])

    mqtt._client.publish.assert_has_calls(
        [
            call(
                "homeassistant/sensor/test_CA-FE-CA-FE-CA-FE/state",
                payload='{"battery": "3", "temperature": "31.2", "conductivity": "123"}',
            ),
            call(
                "homeassistant/sensor/test2_CA-FE-CA-FE-CA-FE/state",
                payload='{"temperature": "12.3", "battery_voltage": "2.345", "movement_counter": "123"}',
            ),
        ]
    )


@patch("config.HumpuukiConfig")
@patch("paho.mqtt.client.Client")
def test_mqtt_publish_availability(mock_mqtt, mock_c):
    mqtt = MqttBackend(
        host="127.0.0.1",
        port=1883,
        topic_prefix="homeassistant",
        publish_availability=True,
    )
    mqtt.configure()

    plant = MiFloraDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    plant.battery.value = 3
    plant.temperature.value = 31.2
    plant.conductivity.value = 123

    ruuvitag = RuuviDevice(
        name="test2",
        address="CA:FE:CA:FE:CA:FE",
        mqtt=Addict({"publish_availability": False}),
    )

    ruuvitag.battery_voltage.value = 2.345
    ruuvitag.temperature.value = 12.3
    ruuvitag.movement_counter.value = 123

    mqtt.publish([plant, ruuvitag])

    mqtt._client.publish.assert_has_calls(
        [
            call(
                "homeassistant/sensor/test_CA-FE-CA-FE-CA-FE/state",
                payload='{"battery": "3", "temperature": "31.2", "conductivity": "123"}',
            ),
            call(
                "homeassistant/sensor/test_CA-FE-CA-FE-CA-FE/availability",
                payload="online",
                retain=True,
            ),
            call(
                "homeassistant/binary_sensor/test_CA-FE-CA-FE-CA-FE_low_battery/availability",
                payload="offline",
                retain=True,
            ),
            call(
                "homeassistant/sensor/test2_CA-FE-CA-FE-CA-FE/state",
                payload='{"temperature": "12.3", "battery_voltage": "2.345", "movement_counter": "123"}',
            ),
        ]
    )


@pytest.mark.integration_test
def test_integration_mqtt_publish():
    pass
