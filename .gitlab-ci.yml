stages:
  - test
  - build

variables:
  VERSION: ${CI_COMMIT_SHORT_SHA}
  PYTHON_VERSION: "3.9"
  POETRY_VERSION: "1.1.12"
  HELM_VERSION: "3.4.2"
  HADOLINT_VERSION: "v1.9.0"
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .cache/pip

default:
  image: python:${PYTHON_VERSION}
  before_script:
    - python --version
    - pip install poetry==${POETRY_VERSION}
    - poetry install

pytest:
  stage: test
  script:
    - >
      poetry run
      pytest -v
      --junitxml=junit.xml
      --cov-report=term
      --cov=backend
      --cov=device
  artifacts:
    when: always
    reports:
      junit: junit.xml
  coverage: '/TOTAL.*\s+(\d+%)$/'

black:
  stage: test
  script:
    - poetry run black --check --diff .

isort:
  stage: test
  script:
    - poetry run isort --diff .

mypy:
  stage: test
  script:
    - >
      poetry run
      mypy
      --junit-xml junit.xml
      --cobertura-xml-report coverage
      -p humpuuki
  artifacts:
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/cobertura.xml
      junit: junit.xml

smoke:
  variables:
    HUMPUUKI_LOGGING__LEVEL: debug
    HUMPUUKI_CHECK_MODE: "true"
  script:
    - poetry run python humpuuki.py

helm:
  stage: test
  image:
    name: alpine/helm:${HELM_VERSION}
    entrypoint: [""]
  inherit:
    default: false
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - changes:
        - charts/**
  script:
    - helm lint charts/humpuuki

dockerfile:
  stage: test
  image: hadolint/hadolint:${HADOLINT_VERSION}-debian
  inherit:
    default: false
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - changes:
        - Dockerfile
  script:
    - hadolint Dockerfile

requirements:
  stage: build
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
  script:
    - >
      poetry export
      --without-hashes
      --format requirements.txt
      --output requirements.txt
  artifacts:
    paths:
      - requirements.txt

container:
  stage: build
  image: registry.gitlab.com/gdunstone/docker-buildx-qemu
  inherit:
    default: false
  needs:
    - job: requirements
      artifacts: true
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
  services:
    - name: docker:dind
      entrypoint: ["env", "-u", "DOCKER_HOST"]
      command: ["dockerd-entrypoint.sh"]
  variables:
    PLATFORMS: linux/arm/v7,linux/arm64/v8,linux/amd64
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    # See https://github.com/docker-library/docker/pull/166
    DOCKER_TLS_CERTDIR: ""
  before_script:
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
    - update-binfmts --enable
    - docker buildx create --driver docker-container --use
    - docker buildx inspect --bootstrap
  script:
    - >
      docker buildx build
      --progress plain
      --push --pull
      --cache-from type=registry,ref=${CI_REGISTRY_IMAGE}/cache:latest
      --cache-to type=registry,ref=${CI_REGISTRY_IMAGE}/cache:latest,mode=max
      --platform ${PLATFORMS}
      --tag ${CI_REGISTRY_IMAGE}:${VERSION}
      --tag ${CI_REGISTRY_IMAGE}:latest .

chart:
  stage: build
  image:
    name: alpine/helm:${HELM_VERSION}
    entrypoint: [""]
  inherit:
    default: false
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
  before_script:
    - apk add git
    - helm plugin install https://github.com/chartmuseum/helm-push
    - helm repo add --username gitlab-ci-token --password $CI_JOB_TOKEN humpuuki ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/stable
  script:
    - CHART=$(helm package charts/humpuuki --app-version ${VERSION} | cut -d' ' -f8)
    - helm cm-push ${CHART} humpuuki
