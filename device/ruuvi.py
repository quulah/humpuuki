from __future__ import annotations

import logging
import math
from typing import Optional

import pendulum
from ruuvitag import RuuviTag

from .base import HumpuukiDevice
from .sensor import BinarySensor, Sensor

log = logging.getLogger("humpuuki")


class RuuviDevice(HumpuukiDevice):
    manufacturer: str = "Ruuvi Innovations Ltd"
    model: str = "RuuviTag"

    temperature: Sensor = Sensor(unit="°C")
    humidity: Sensor = Sensor(unit="%")
    pressure: Sensor = Sensor(unit="Pa")
    acceleration_x: Sensor = Sensor(unit="G")
    acceleration_y: Sensor = Sensor(unit="G")
    acceleration_z: Sensor = Sensor(unit="G")
    battery_voltage: Sensor = Sensor(unit="V")
    tx_power: Sensor = Sensor(unit="dBm")
    movement_counter: Sensor = Sensor()
    measurement_sequence: Sensor = Sensor()

    # Calculated values
    equilibrium_vapor_pressure: Sensor = Sensor(unit="Pa")
    absolute_humidity: Sensor = Sensor(unit="g/m³")
    dew_point: Sensor = Sensor(unit="°C")
    air_density: Sensor = Sensor(unit="kg/m³")
    total_acceleration: Sensor = Sensor(unit="mG")

    low_battery: BinarySensor = BinarySensor()
    movement_detected: BinarySensor = BinarySensor()
    low_battery_limit: float = 2.3

    def parse(self, raw: Optional[bytes] = None) -> None:
        log.debug(f"Parsing raw data: {self.name} ({self.address}): {raw!r}")

        try:
            tag = RuuviTag.parse(self.address, raw)

            if self.movement_counter.set:
                self.movement_detected.value = (
                    tag.movement_counter > self.movement_counter.value
                )
            else:
                self.movement_detected.value = self.movement_detected.default

            self.temperature.value = tag.temperature
            self.humidity.value = tag.humidity
            self.pressure.value = tag.pressure
            self.acceleration_x.value = tag.acceleration_x
            self.acceleration_y.value = tag.acceleration_y
            self.acceleration_z.value = tag.acceleration_z
            self.battery_voltage.value = tag.battery_voltage
            self.tx_power.value = tag.tx_power
            self.movement_counter.value = tag.movement_counter
            self.measurement_sequence.value = tag.measurement_sequence

            if self.battery_voltage.set:
                self.low_battery.value = self.battery_voltage.value < self.low_battery_limit  # type: ignore[operator]
            else:
                self.low_battery.value = self.low_battery.default

            self.calculate_values()

            self.updated = pendulum.now()
        except Exception as e:
            log.warning(f"Failed to parse raw data: {self.name} ({self.address})")
            log.debug(f"Parsing exception: {e}")

    # From: https://github.com/Scrin/RuuviCollector/blob/master/src/main/java/fi/tkgwf/ruuvi/utils/MeasurementValueCalculator.java
    def calculate_values(self) -> None:
        if self.temperature.set:
            self.equilibrium_vapor_pressure.value = 611.2 * math.exp(
                17.67 * self.temperature.value / (243.5 + self.temperature.value)  # type: ignore[operator]
            )

            if self.humidity.set:
                self.absolute_humidity.value = (
                    self.equilibrium_vapor_pressure.value  # type: ignore[operator]
                    * self.humidity.value
                    * 0.021674
                    / (273.15 + self.temperature.value)  # type: ignore[operator]
                )

                v = math.log(
                    self.humidity.value / 100 * self.equilibrium_vapor_pressure.value / 611.2  # type: ignore[operator]
                )
                self.dew_point.value = -243.5 * v / (v - 17.67)

                if self.pressure.set:
                    self.air_density.value = (
                        1.2929
                        * 273.15
                        / (self.temperature.value + 273.15)  # type: ignore[operator]
                        * (
                            self.pressure.value  # type: ignore[operator]
                            - 0.3783  # type: ignore[operator]
                            * self.humidity.value
                            / 100
                            * self.equilibrium_vapor_pressure.value
                        )
                        / 101300
                    )

        if (
            self.acceleration_x.set
            and self.acceleration_y.set
            and self.acceleration_z.set
        ):
            self.total_acceleration.value = math.sqrt(
                self.acceleration_x.value * self.acceleration_x.value  # type: ignore[operator]
                + self.acceleration_y.value * self.acceleration_y.value  # type: ignore[operator]
                + self.acceleration_z.value * self.acceleration_z.value  # type: ignore[operator]
            )
