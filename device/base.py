import logging
from datetime import datetime
from typing import Any, Dict, Optional, Type, TypeVar, Union

import pendulum
from addict import Dict as Addict
from bluepy.btle import Peripheral
from pydantic import BaseModel, Field, PrivateAttr

from .sensor import BinarySensor, Sensor

log = logging.getLogger("humpuuki")


class HumpuukiDevice(BaseModel):
    name: str
    address: str
    manufacturer: str
    model: str

    # FIXME These could probably be an instance of Scan
    interval: int = 10
    retries: int = 0
    cache_time: int = 0

    peripheral: bool = False
    _peripheral: Optional[Peripheral] = PrivateAttr()

    # FIXME: These could be the backend BaseModels, but that causes a circular import
    mqtt: Addict = Field(default_factory=Addict)
    logger: Addict = Field(default_factory=Addict)

    updated: pendulum.DateTime = Field(default_factory=pendulum.now)

    def __init__(self, **data: Any) -> None:
        super().__init__(**data)

        if self.peripheral:
            self._peripheral = Peripheral()
        else:
            self._peripheral = None

    def __str__(self) -> str:
        values = [f"{k}={v}" for k, v in self.all_sensors.items() if v.set]

        return f"{self.name} ({self.address}): {', '.join(values)}"

    def update(self, raw: Optional[bytes] = None) -> None:
        if raw:
            self.parse(raw)
        else:
            if self.updated.diff().in_seconds() > self.cache_time:
                log.debug(
                    f"Reached cache_time limit for: {self.name}. Resetting sensor states."
                )

                self.reset()

    def parse(self, raw: bytes) -> None:
        pass

    def reset(self) -> None:
        for sensor in list(self.all_sensors.keys()):
            setattr(getattr(self, sensor), "value", None)

    @property
    def klass(self) -> str:
        return type(self).__name__.removesuffix("Device").lower()

    @property
    def id(self) -> str:
        return f"{self.name}_{self.address.upper().replace(':', '-')}"

    @property
    def sensors(self) -> Dict[str, Sensor]:
        return {k: v for (k, v) in vars(self).items() if type(v) == Sensor}

    @property
    def binary_sensors(self) -> Dict[str, Sensor]:
        return {k: v for (k, v) in vars(self).items() if type(v) == BinarySensor}

    @property
    def all_sensors(self) -> Dict[str, Sensor]:
        return self.sensors | self.binary_sensors


HumpuukiDevice.update_forward_refs()
