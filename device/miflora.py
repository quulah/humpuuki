import logging
from typing import Optional

import pendulum
from bluepy.btle import BTLEException

from .base import HumpuukiDevice
from .sensor import BinarySensor, Sensor

log = logging.getLogger("humpuuki")


class MiFloraDevice(HumpuukiDevice):
    manufacturer: str = "Xiaomi"
    model: str = "MiFlora"
    firmware: str = ""
    peripheral: bool = True

    interval: int = 300
    retries: int = 3

    battery: Sensor = Sensor(unit="%")
    temperature: Sensor = Sensor(unit="°C")
    light: Sensor = Sensor(unit="lux")
    moisture: Sensor = Sensor(unit="%")
    conductivity: Sensor = Sensor(unit="µS/cm")

    low_battery: BinarySensor = BinarySensor()
    low_battery_limit: int = 10

    def update(self, raw: Optional[bytes] = None) -> None:
        try:
            if self._peripheral:
                self._peripheral.connect(self.address.lower())

                # From: https://github.com/kipe/miplant/blob/master/miplant/miplant.py
                self._peripheral.writeCharacteristic(
                    0x33, bytearray([0xA0, 0x1F]), withResponse=True
                )

                # hardware: battery + firmware
                hardware = bytearray(self._peripheral.readCharacteristic(0x38))
                # sensors: temperature + light + moisture + conductivity
                sensors = bytearray(self._peripheral.readCharacteristic(0x35))

                raw = bytes(hardware + sensors)
            else:
                # This is mostly for the benefit of Mypy, but checking for sanity
                log.error(f"Peripheral device not set for {self.name}")
        except BTLEException as e:
            log.warning(f"Failed to read from {self.name} ({self.address}): {e}.")
        finally:
            try:
                if self._peripheral:
                    self._peripheral.disconnect()
            except:
                pass

            super().update(raw)

    def parse(self, raw: bytes) -> None:
        log.debug(f"Parsing raw data: {self.name} ({self.address}): {raw!r}")

        try:
            # From: https://github.com/kipe/miplant/blob/master/miplant/miplant.py
            hardware = raw[0:7]
            self.battery.value = hardware[0]
            self.firmware = hardware[2:7].decode()

            sensors = raw[7:]
            self.temperature.value = float(sensors[1] * 256 + sensors[0]) / 10
            self.light.value = sensors[4] * 256 + sensors[3]
            self.moisture.value = sensors[7]
            self.conductivity.value = sensors[9] * 256 + sensors[8]

            if self.battery.set:
                self.low_battery.value = self.battery.value < self.low_battery_limit
            else:
                self.low_battery.value = self.low_battery.default

            self.updated = pendulum.now()
        except Exception as e:
            log.warning(f"Failed to parse raw data: {self.name} ({self.address})")
            log.debug(f"Parsing exception: {e}")
