from typing import Any, Dict, Optional, Type, TypeVar, Union

from pydantic import BaseModel


class Sensor(BaseModel):
    unit: Optional[str] = None
    value: Union[int, float, None] = None

    def __str__(self) -> str:
        if self.set:
            return f"{self.value}{' ' + self.unit if self.unit else ''}"

        return ""

    @property
    def set(self) -> bool:
        # This doesn't satisfy Mypy yet, since it's wrapped in
        # a function https://github.com/python/mypy/issues/5206
        return isinstance(self.value, (int, float))


class BinarySensor(BaseModel):
    value: Optional[bool] = None

    on: str = "ON"
    off: str = "OFF"
    default: bool = False

    def __str__(self) -> str:
        if self.set:
            return self.on if self.value else self.off

        return ""

    @property
    def set(self) -> bool:
        # This doesn't satisfy Mypy yet, since it's wrapped in
        # a function https://github.com/python/mypy/issues/5206
        return isinstance(self.value, bool)
