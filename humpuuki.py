#!/usr/bin/env python3

import logging

from scanner.base import HumpuukiScanner

log = logging.getLogger("humpuuki")


def main() -> None:
    scanner = HumpuukiScanner()
    scanner.run()


if __name__ == "__main__":
    main()
