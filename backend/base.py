import logging
from typing import Any, Dict, List

from pydantic import BaseModel

from device.base import HumpuukiDevice

log = logging.getLogger("humpuuki")


class HumpuukiBackend(BaseModel):
    def __str__(self) -> str:
        return self.klass

    def publish(self, data: List[HumpuukiDevice]) -> None:
        pass

    def configure(self) -> None:
        pass

    @property
    def klass(self) -> str:
        return type(self).__name__.removesuffix("Backend").lower()
