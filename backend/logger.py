import logging
from typing import Any, Callable, Dict, List, Optional

from pydantic import PrivateAttr

import device.base

from .base import HumpuukiBackend

log = logging.getLogger("humpuuki")


class LoggerBackend(HumpuukiBackend):
    level: str = "info"

    # FIXME: If Callable -> https://github.com/python/mypy/issues/2427
    _output: Any = PrivateAttr()

    def configure(self) -> None:
        level_fn = str(self.level).lower()
        self._output = getattr(log, level_fn)

    def publish(self, data: List[device.base.HumpuukiDevice]) -> None:
        for device in data:
            payload = [x.value for x in device.sensors.values() if x.set]

            if payload:
                self._output(f"Data: {device}")
            else:
                log.debug(
                    f"Logger empty payload for sensors {device.name} ({device.address})"
                )
