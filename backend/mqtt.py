import json
import logging
from typing import Any, Dict, List, Optional

import paho.mqtt.client as mqtt
from pydantic import PrivateAttr

import config
import device.base

from .base import HumpuukiBackend

log = logging.getLogger("humpuuki")


class MqttBackend(HumpuukiBackend):
    host: str
    port: int = 1883
    topic_prefix: str = "humpuuki"
    publish_availability: bool = False

    _client: mqtt.Client = PrivateAttr()

    def configure(self) -> None:
        self._client = mqtt.Client()
        self._client.connect(self.host, self.port)

        for device in config.c.devices():  # type: ignore[attr-defined]
            topic = f"{self.topic_prefix}/sensor/{device.id}"
            state_topic = f"{topic}/state"

            device_identifiers = {
                "identifiers": [device.address],
                "manufacturer": device.manufacturer,
                "model": device.model,
                "name": device.name,
            }

            for name, sensor in device.sensors.items():
                config_topic = f"{topic}_{name}/config"

                payload = {
                    "state_topic": state_topic,
                    "unique_id": f"{device.id}_{name}",
                    "name": f"{device.klass}_{device.name}_{name}",
                    "device": device_identifiers,
                    "unit_of_measurement": sensor.unit,
                    "value_template": f"{{{{ value_json.{name} }}}}",
                }

                if (
                    device.mqtt.publish_availability
                    or self.publish_availability
                    and device.mqtt.publish_availability == {}
                ):
                    availability_topic = f"{topic}_{name}/availability"
                    payload["availability_topic"] = availability_topic

                log.debug(f"MQTT config: ({config_topic}): {payload}")
                self._client.publish(
                    config_topic, payload=json.dumps(payload), retain=True
                )

            topic = f"{self.topic_prefix}/binary_sensor/{device.id}"

            for name, sensor in device.binary_sensors.items():
                state_topic = f"{topic}_{name}"
                config_topic = f"{topic}_{name}/config"

                payload = {
                    "state_topic": state_topic,
                    "unique_id": f"{device.id}_{name}",
                    "name": f"{device.klass}_{device.name}_{name}",
                    "device": device_identifiers,
                    "value_template": f"{{{{ value_json.state }}}}",
                }

                if (
                    device.mqtt.publish_availability
                    or self.publish_availability
                    and device.mqtt.publish_availability == {}
                ):
                    availability_topic = f"{topic}_{name}/availability"
                    payload["availability_topic"] = availability_topic

                log.debug(f"MQTT config: ({config_topic}): {payload}")
                self._client.publish(
                    config_topic, payload=json.dumps(payload), retain=True
                )

    def publish(self, data: List[device.base.HumpuukiDevice]) -> None:
        self._client.reconnect()

        for device in data:
            topic = f"{self.topic_prefix}/sensor/{device.id}"
            state_topic = f"{topic}/state"

            payload: Dict[str, str] = {}

            for name, sensor in device.sensors.items():
                if sensor.set:
                    payload[name] = f"{sensor.value}"

            if payload:
                log.debug(f"MQTT publish: ({state_topic}): {payload}")
                self._client.publish(state_topic, payload=json.dumps(payload))
            else:
                log.debug(
                    f"MQTT empty payload for sensors {device.name} ({device.address})"
                )

            if (
                device.mqtt.publish_availability
                or self.publish_availability
                and device.mqtt.publish_availability == {}
            ):
                availability_topic = f"{topic}/availability"
                self._client.publish(
                    availability_topic,
                    payload="online" if payload else "offline",
                    retain=True,
                )

            topic = f"{self.topic_prefix}/binary_sensor/{device.id}"

            for name, sensor in device.binary_sensors.items():
                state_topic = f"{topic}_{name}"
                availability_topic = f"{topic}_{name}/availability"

                payload = {}

                if sensor.set:
                    payload = {"state": f"{sensor}"}

                if payload:
                    log.debug(f"MQTT publish: ({state_topic}): {payload}")
                    self._client.publish(state_topic, payload=json.dumps(payload))
                else:
                    log.debug(
                        f"MQTT no state for binary sensor {device.name} ({device.address}) {name}"
                    )

                if (
                    device.mqtt.publish_availability
                    or self.publish_availability
                    and device.mqtt.publish_availability == {}
                ):
                    availability_topic = f"{topic}_{name}/availability"
                    self._client.publish(
                        availability_topic,
                        payload="online" if payload else "offline",
                        retain=True,
                    )

        self._client.disconnect()
