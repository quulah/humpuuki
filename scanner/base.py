import logging
import time
from typing import Any, Dict, List

import schedule
from bluepy.btle import BTLEException, DefaultDelegate, ScanEntry, Scanner

from config import c  # type: ignore[attr-defined]
from device.base import HumpuukiDevice

log = logging.getLogger("humpuuki")


class HumpuukiScanner:
    peripherals: List[HumpuukiDevice] = []
    beacons: List[HumpuukiDevice] = []

    def __init__(self) -> None:
        if c.devices():
            log.info(f"Devices: {', '.join(x.name for x in c.devices())}")

            self.peripherals = [x for x in c.devices() if x.peripheral]
            self.beacons = [x for x in c.devices() if not x.peripheral]

            # Schedule each peripheral device separately with their own interval
            for device in self.peripherals:
                # TODO: This is not very pretty, but peripherals
                # don't receive raw data in the update() so calling with None,
                # so the interface is the same as for other devices.
                #
                # Actual data will get read from the peripheral in the update() call
                schedule.every(device.interval).seconds.do(device.update, raw=None)

            # Schedule a scan for each unique interval, with all the beacons
            # with that interval
            intervals = list(set([x.interval for x in self.beacons]))

            for interval in intervals:
                devices = {
                    x.address.lower(): x for x in self.beacons if x.interval == interval
                }
                schedule.every(interval).seconds.do(self.scan, devices=devices)

            log.debug(f"Scheduled jobs: {schedule.jobs}")
        else:
            log.warning("No devices configured!")

        if c.backends():
            log.info(f"Backends: {', '.join([str(x) for x in c.backends()])}")

            if not c.check_mode:
                for backend in c.backends():
                    backend.configure()
            else:
                log.info("Check mode enabled. Will not configure backends.")
        else:
            log.warning("No backends configured!")

    def scan(self, devices: Dict[str, HumpuukiDevice]) -> None:
        log.debug(f"Scanner checking data for devices: {', '.join(devices.keys())}")

        data = {}

        try:
            scanner = Scanner()
            entries = scanner.scan(timeout=c.scan.timeout)

            data = {x.addr: x.rawData for x in entries if x.addr in devices}
        except BTLEException as e:
            log.warning(f"Error occurred when scanning: {e}.")

        if data:
            log.debug(f"Scanner returned data for devices: {', '.join(data.keys())}")

        for address, device in devices.items():
            raw = data.get(address, None)
            device.update(raw=raw)

    def run(self) -> None:
        while True:
            schedule.run_pending()

            if c.check_mode:
                log.info(f"Check mode enabled, exiting!")
                break
            # TODO: This will end up publishing device states every second
            # even though the scan interval suggests otherwise. So, we ehd up sending
            # stale data until and if it gets updated every interval.
            #
            # One solution is to let this sleep() be configurable, or just move publish()
            # to the scheduled things above. Unclear if the publish time should be configurable
            # globally, per backend, per device or something else so leaving this as-is for now.
            for backend in c.backends():
                backend.publish(c.devices())

            time.sleep(1)
